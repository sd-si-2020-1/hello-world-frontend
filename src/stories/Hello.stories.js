// Hello.stories.js

import React from 'react';
import { Hello } from '../Hello';

export default {
  title: 'Components/Hello',
  component: Hello,
}

export const Primary = () => <Hello saudacao='Olá!'/>;