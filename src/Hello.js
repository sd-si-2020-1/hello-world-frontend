import React, { useState } from 'react';

export function Hello(props) {
  const [saudacao, setSaudacao] = useState(props.saudacao)
  const handleClick = () => {
    // console.log('clicou')
    setSaudacao('Usando o state')
  }
  return (
    <>
      <h1>{saudacao}</h1>
      <HelloButton
        clicar={handleClick}
      />
    </>
  )
}

export function HelloButton(props) {
  return (
    <button 
      onClick={props.clicar}
    >
      Clique aqui
      </button>
  )
}

export default Hello